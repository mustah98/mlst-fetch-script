import time
import math
import textwrap
from collections import Counter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
option = webdriver.ChromeOptions()
# option.add_argument('headless')


PATH = "/mnt/c/Users/Mustafa/Desktop/Arbeit/Silver/Cryptococous/new_mlst_fetch/chromedriver.exe"
driver = webdriver.Chrome(PATH,options=option)

driver.get("https://mlst.mycologylab.org/page/Allele_Search")
time.sleep(5)

search = driver.find_element_by_xpath('//*[@id="mat-select-6"]/div/div[2]')
search.click()
time.sleep(1)
organism = driver.find_element_by_xpath('//*[@id="mat-option-11"]/span')
organism.click()
time.sleep(1)
start_search = driver.find_element_by_xpath('//*[@id="search-btn"]/span')
start_search.click()
time.sleep(1)

items = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/div[1]/mat-paginator/div/div/div[1]/mat-form-field/div/div[1]/div')
items.click()
time.sleep(1)
select_item = driver.find_element_by_xpath('//*[@id="mat-option-78"]/span')
select_item.click()
time.sleep(1)

sets = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/div[1]/mat-paginator/div/div/div[2]/div')
sets = sets.get_attribute("textContent")


items_page = str(sets).split(" ")[3]
items_all = str(sets).split(" ")[5]
pages = math.ceil(int(items_all)/int(items_page))
last_page_elem = int(items_all)-(int(items_page)*(pages-1))



list_of_locuses = []
with open("multilocus.fasta", "w") as write_file:
    for j in range(1,pages+1):
        if j < pages:
            for i in range(1,int(items_page)+1):
                locus = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/perfect-scrollbar/div/div[1]/div/table/tbody/tr['+str(i)+']/td[4]/summary-generator/text-summary-field')
                locus = locus.get_attribute("textContent")
                if locus not in list_of_locuses:
                    list_of_locuses.append(locus)
                allel_type = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/perfect-scrollbar/div/div[1]/div/table/tbody/tr['+str(i)+']/td[5]/summary-generator/text-summary-field')
                allel_type = allel_type.get_attribute("textContent")
                sequence = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/perfect-scrollbar/div/div[1]/div/table/tbody/tr['+str(i)+']/td[6]/summary-generator/n-summary-field/div/a/overflow-tooltip/div')
                sequence = sequence.get_attribute("textContent")
                sequence = '\n'.join(textwrap.wrap(sequence, 80))
                write_file.write(">"+locus +"-"+allel_type + "\n" + sequence + "\n")
                print(locus)
                print(allel_type)
                print(sequence)
            swich_page = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/div[1]/mat-paginator/div/div/div[2]/button[3]')
            swich_page.click()
            time.sleep(2)
        else:
            for i in range(int(last_page_elem)):
                locus = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/perfect-scrollbar/div/div[1]/div/table/tbody/tr['+str(i+1)+']/td[4]/summary-generator/text-summary-field')
                locus = locus.get_attribute("textContent")
                allel_type = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/perfect-scrollbar/div/div[1]/div/table/tbody/tr['+str(i+1)+']/td[5]/summary-generator/text-summary-field')
                allel_type = allel_type.get_attribute("textContent")
                sequence = driver.find_element_by_xpath('//*[@id="summary-content"]/mat-card/mat-card-content/perfect-scrollbar/div/div[1]/div/table/tbody/tr['+str(i+1)+']/td[6]/summary-generator/n-summary-field/div/a/overflow-tooltip/div')
                sequence = sequence.get_attribute("textContent")
                sequence = '\n'.join(textwrap.wrap(sequence, 80))
                write_file.write(">"+locus +"-"+allel_type + "\n" + sequence + "\n")
                # print(locus)
                # print(allel_type)
                # print(sequence)
write_file.close()
print("finished fetching")
driver.quit()
#
with open("multilocus.fasta", "r") as readfile:
    counter = 0
    lines = readfile.readlines()
    for locus in  list_of_locuses:
        with open(locus+".fasta", "w") as writefile:
            for i in range (counter,len(lines)):
                if lines[i][0] == ">":
                    head = lines[i].split(">")[1].split("-")[0]
                    if head == locus:
                        writefile.write(lines[i])
                        counter += 1
                        continue
                    else:
                        break
                else:
                    writefile.write(lines[i])
                    counter += 1
            print(counter)
            writefile.close()
